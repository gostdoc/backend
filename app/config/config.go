package config

type Config interface {
	ReadFromFile(path string)
	StringParamByName(string) string
	IntParamByName(string) int
	BoolParamByName(string) bool
}
