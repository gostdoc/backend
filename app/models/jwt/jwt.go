package jwt_models

import (
	"strings"
	"time"

	"github.com/golang-jwt/jwt/v5"
	"gitlab.com/backend/gost-doc/app/config"
	"gitlab.com/backend/gost-doc/app/models"
	"gitlab.com/backend/gost-doc/app/models/errcore"
	"gitlab.com/f4lc09/logger"
)

type JWTService struct {
	signingKey string
	tokenTTL   time.Duration
}

func NewJWTService(cfg config.Config) *JWTService {
	jwtService := &JWTService{}
	jwtService.signingKey = cfg.StringParamByName("signingKey")
	jwtService.tokenTTL = time.Duration(cfg.IntParamByName("tokenTTL"))
	return jwtService
}

func (s *JWTService) GenerateTokenFromUserData(id int64) (string, error) {
	tokenTTL := time.Hour * time.Duration(s.tokenTTL)
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, &models.AuthToken{
		RegisteredClaims: jwt.RegisteredClaims{
			ExpiresAt: jwt.NewNumericDate(time.Now().Add(tokenTTL)),
		},
		UserId: id,
	})

	tokenString, err := token.SignedString([]byte(s.signingKey))
	if err != nil {
		return "", err
	}

	return tokenString, nil
}

func (s *JWTService) ExtractBearerToken(header string) (string, error) {
	if header == "" {
		logger.Log("токен пустой")
		return "", errcore.InvalidHeaderError
	}

	jwtToken := strings.Split(header, " ")
	if len(jwtToken) != 2 {
		logger.Log("неверная структура токена")
		return "", errcore.InvalidHeaderError
	}

	return jwtToken[1], nil
}

func (s *JWTService) ParseToken(jwtToken string) (*models.AuthToken, error) {
	token, err := jwt.ParseWithClaims(jwtToken, &models.AuthToken{}, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			logger.Log("неверная сигнатура токена")
			return nil, errcore.InvalidTokenError
		}
		return []byte(s.signingKey), nil
	})

	if err != nil {
		logger.Log("токен не соответствует структуре")
		return nil, errcore.InvalidTokenError
	}

	claims, ok := token.Claims.(*models.AuthToken)
	if !ok || !token.Valid {
		logger.Log("токен не соответствует структуре")
		return nil, errcore.InvalidTokenError
	}

	return claims, nil
}

func (s *JWTService) GetRefreshTokenTTL() int {
	refreshTTL := 24 * 60 * 60 * s.tokenTTL
	return int(refreshTTL)
}
