package models

type UserTokens struct {
	RefreshToken string `json:"refresh_token,omitempty"`
	AccessToken  string `json:"access_token,omitempty"`
}

type RefreshTokenFromDB struct {
	Id    *int64 `json:"id"`
	Token string `json:"refresh_token"`
}
