package postgresql

import (
	"context"
	"errors"
	"fmt"
	"sync"

	"github.com/jackc/pgx/v5"
	"github.com/jackc/pgx/v5/pgconn"
	"gitlab.com/backend/gost-doc/app/config"
	"gitlab.com/backend/gost-doc/app/models/errcore"
	"gitlab.com/f4lc09/logger"
)

type postgresql struct {
	conn  *pgx.Conn
	mutex sync.Mutex
}

func NewPostgresqlRepository() *postgresql {
	return new(postgresql)
}

func (p *postgresql) InitConnection(cfg config.Config) {
	dsn := cfg.StringParamByName("dsn")
	if dsn == "" {
		logger.Fatalf("dsn is not specified in config")
	}

	conn, err := pgx.Connect(context.Background(), dsn)
	if err != nil {
		logger.Fatalf("unable to connect to database: %v\n", err)
	}
	logger.Log("connected to database")

	p.conn = conn
	p.mutex = sync.Mutex{}
}

func (p *postgresql) ExecSP(spName string, params ...any) ([]byte, *errcore.DBError) {
	p.mutex.Lock()
	defer p.mutex.Unlock()
	var (
		dbError   *errcore.DBError
		rawResult []byte
	)

	sql := fmt.Sprintf("select %s(", spName)
	for i := range params {
		if i == 0 {
			sql += "$1"
			continue
		}
		sql += fmt.Sprintf(", $%d", i+1)
	}
	sql += ");"

	row := p.conn.QueryRow(context.Background(), sql, params...)
	err := row.Scan(&rawResult)
	if err != nil {
		dbError = p.makeDBErr(err)

		return nil, dbError
	}

	return rawResult, dbError
}

func (p *postgresql) Close() error {
	return p.conn.Close(context.Background())
}

func (p *postgresql) makeDBErr(err error) *errcore.DBError {
	var pgErr = new(pgconn.PgError)

	dbErr := &errcore.DBError{}

	errors.As(err, &pgErr)
	if pgErr.Code != "P0001" {
		logger.Log(err.Error())
		dbErr.Internal = true
	}

	dbErr.Message = pgErr.Message
	return dbErr
}
