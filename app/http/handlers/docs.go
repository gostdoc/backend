package handlers

import (
	"encoding/json"
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/backend/gost-doc/app/models"
	"gitlab.com/backend/gost-doc/app/models/errcore"
	jwt_models "gitlab.com/backend/gost-doc/app/models/jwt"
	"gitlab.com/backend/gost-doc/app/repository"
	"gitlab.com/f4lc09/logger"
)

type docHandler struct {
	db         repository.Driver
	jwtService *jwt_models.JWTService
}

func NewDocHandler(db repository.Driver, router *gin.RouterGroup, jwtService *jwt_models.JWTService) *docHandler {
	handler := &docHandler{
		db:         db,
		jwtService: jwtService,
	}

	docs := router.Group("/docs")

	docs.POST(`/create`, AuthMiddleware(handler.jwtService), handler.CreateNewDocument)
	docs.GET(`/`, AuthMiddleware(handler.jwtService), handler.GetUserDocuments)
	// docs.DELETE(`/`, authHandler.TokenAuthMiddleware, handler.GetUserDocuments)
	// favourite
	// rename
	docs.POST(`/change_privacy`, AuthMiddleware(handler.jwtService), handler.ChangePrivacySettings)
	// change share settings
	docs.GET("/:doc_ref", handler.wshandler)

	return handler
}

// @Summary      Создает новый документ.
// @Description  Нужно указать `название`
// @Tags         Docs
// @Accept       json
// @Produce      json
// @Param        request body models.CreateDocRequest true "Запрос"
// @Success      200  {number}  int
// @Failure      400  {object}  errcore.ErrorResponse
// @Failure      500  {object}  errcore.ErrorResponse
// @Security     ApiKeyAuth
// @Router       /api/docs/create [post]
func (h *docHandler) CreateNewDocument(c *gin.Context) {
	var (
		req models.CreateDocRequest
	)
	uid := c.GetInt64("uid")

	if err := c.BindJSON(&req); err != nil {
		logger.Error(err)
		c.JSON(http.StatusBadRequest, errcore.InvalidJsonError)
		return
	}

	dbResult, dbErr := h.db.ExecSP("create_document", uid, req)
	if dbErr != nil {
		setDBError(c, dbErr)
		return
	}
	docRef := string(dbResult)

	c.JSON(http.StatusOK, docRef)
}

// @Summary      Получает списсок документов, доступных пользователю.
// @Tags         Docs
// @Accept       json
// @Produce      json
// @Success      200  {array}  models.Document
// @Failure      400  {object}  errcore.ErrorResponse
// @Failure      500  {object}  errcore.ErrorResponse
// @Security     ApiKeyAuth
// @Router       /api/docs [get]
func (h *docHandler) GetUserDocuments(c *gin.Context) {
	uid := c.GetInt64("uid")

	dbResult, dbErr := h.db.ExecSP("get_documents_by_user", uid)
	if dbErr != nil {
		setDBError(c, dbErr)
		return
	}
	docs := []models.Document{}
	err := json.Unmarshal(dbResult, &docs)
	if err != nil {
		logger.Errorf("cannot unmarshal docs: %v", err)
	}

	c.JSON(http.StatusOK, docs)
}

// func (h *docHandler) DeleteUserDocument(c *gin.Context) {
// }

// @Summary      Изменить настройки приватности.
// @Tags         Docs
// @Accept       json
// @Produce      json
// @Param        request body models.ChangePrivacyRequest true "Запрос"
// @Success      200
// @Failure      400  {object}  errcore.ErrorResponse
// @Failure      500  {object}  errcore.ErrorResponse
// @Security     ApiKeyAuth
// @Router       /api/docs/share [post]
func (h *docHandler) ChangePrivacySettings(c *gin.Context) {
	var (
		req models.ChangePrivacyRequest
	)
	uid := c.GetInt64("uid")

	if err := c.BindJSON(&req); err != nil {
		logger.Error(err)
		c.JSON(http.StatusBadRequest, errcore.InvalidJsonError)
		return
	}

	_, dbErr := h.db.ExecSP("update_document_status", uid, req)
	if dbErr != nil {
		setDBError(c, dbErr)
		return
	}

	c.JSON(http.StatusNoContent, nil)

}
