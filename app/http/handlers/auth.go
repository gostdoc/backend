package handlers

import (
	"encoding/json"
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/backend/gost-doc/app/models"
	"gitlab.com/backend/gost-doc/app/models/errcore"
	jwt_models "gitlab.com/backend/gost-doc/app/models/jwt"
	"gitlab.com/backend/gost-doc/app/repository"
	"gitlab.com/f4lc09/logger"
)

type authHandler struct {
	db         repository.Driver
	jwtService *jwt_models.JWTService
}

func NewAuthHandler(db repository.Driver, router *gin.RouterGroup, jwtService *jwt_models.JWTService) *authHandler {
	handler := &authHandler{
		db:         db,
		jwtService: jwtService,
	}

	auth := router.Group("/auth")

	auth.POST(`/registration`, handler.Registration)
	auth.POST(`/login`, handler.Login)
	auth.GET(`/refresh`, AuthMiddleware(jwtService), handler.UpdateTokens)

	return handler
}

// @Summary      Позволяет пользователю создать новый аккаунт
// @Description  Для регистрации требуется `email`, `username` и `пароль`. Ставит рефреш как httpOnly.
// @Tags         Auth
// @Accept       json
// @Produce      json
// @Cookie       refresh_token
// @Param        request body models.RegistrationRequest true "Request"
// @Success      200  {string}  string
// @Failure      400  {object}  errcore.ErrorResponse
// @Failure      500  {object}  errcore.ErrorResponse
// @Router       /api/auth/registration [post]
func (h *authHandler) Registration(c *gin.Context) {
	var (
		userData models.RegistrationRequest
	)

	if err := c.BindJSON(&userData); err != nil {
		logger.Error(err)
		c.JSON(http.StatusBadRequest, errcore.InvalidJsonError)
		return
	}

	dbResult, dbErr := h.db.ExecSP("create_user", userData)
	if dbErr != nil {
		setDBError(c, dbErr)
		return
	}

	tokens, err := h.MakeTokensFromDBResponse(dbResult)
	if err != nil {
		c.JSON(http.StatusInternalServerError, errcore.InternalServerError)
		return
	}

	h.sendTokens(c, tokens)
}

// @Summary      Позволяет пользователю войти в свой аккаунт.
// @Description  В качестве логина пользователь может использовать `email` или `username`. Также нужно указать пароль. Ставит рефреш как httpOnly.
// @Tags         Auth
// @Accept       json
// @Produce      json
// @Param        request body models.LoginRequest true "Запрос"
// @Success      200  {string}  string
// @Failure      400  {object}  errcore.ErrorResponse
// @Failure      500  {object}  errcore.ErrorResponse
// @Router       /api/auth/login [post]
func (h *authHandler) Login(c *gin.Context) {
	var userData models.LoginRequest

	if err := c.BindJSON(&userData); err != nil {
		logger.Log(err.Error())
		c.JSON(http.StatusBadRequest, errcore.InvalidJsonError)
		return
	}

	dbResult, dbErr := h.db.ExecSP("validate_credentials", userData)
	if dbErr != nil {
		setDBError(c, dbErr)
		return
	}

	tokens, err := h.MakeTokensFromDBResponse(dbResult)
	if err != nil {
		c.JSON(http.StatusInternalServerError, errcore.InternalServerError)

	}

	h.sendTokens(c, tokens)
}

// Refresh godoc
// @Summary      Обновляет RefreshToken, отдает AccessToken.
// @Description  Отдает пользователю два токена взамен на Рефреш Токен. Рефреш должен отправляться через куки.
// @Tags         Auth
// @Produce      json
// @Success      200  {string}  string
// @Failure      400  {object}  errcore.ErrorResponse
// @Failure      500  {object}  errcore.ErrorResponse
// @Security     ApiKeyAuth
// @Router       /api/auth/refresh [get]
func (h *authHandler) UpdateTokens(c *gin.Context) {
	refresh, err := c.Cookie("refresh_token")
	if err != nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, errcore.UnsignedResponse{
			Message: "не указан refresh token",
		})
		return
	}
	uid := c.GetInt64("uid")
	dbResult, dbErr := h.db.ExecSP("update_refresh_token", uid, refresh)
	if dbErr != nil {
		setDBError(c, dbErr)
		return
	}
	tokens, err := h.MakeTokensFromDBResponse(dbResult)
	if err != nil {
		c.JSON(http.StatusInternalServerError, errcore.InternalServerError)

	}
	h.sendTokens(c, tokens)
}

func (h *authHandler) sendTokens(c *gin.Context, tokens *models.UserTokens) {
	c.SetCookie("refresh_token", tokens.RefreshToken, h.jwtService.GetRefreshTokenTTL(), "/", "gostdoc.ru", true, true)
	c.JSON(http.StatusOK, tokens.AccessToken)
}

func setDBError(c *gin.Context, dbErr *errcore.DBError) {
	if dbErr.Internal {
		c.JSON(http.StatusInternalServerError, errcore.InternalServerError)
		return
	}

	c.JSON(http.StatusBadRequest, errcore.ErrorResponse{
		Message: dbErr.Message,
	})
}

func (h *authHandler) MakeTokensFromDBResponse(dbResult []byte) (*models.UserTokens, error) {
	var userToken models.RefreshTokenFromDB
	err := json.Unmarshal(dbResult, &userToken)
	if err != nil {
		logger.Errorf("cannot unmarshal userToken: %v", err)
	}

	token, err := h.jwtService.GenerateTokenFromUserData(int64(*userToken.Id))
	if err != nil {
		logger.Errorf("cannot generate token: %v", err)
		return nil, err
	}

	return &models.UserTokens{
		RefreshToken: userToken.Token,
		AccessToken:  token,
	}, nil
}
