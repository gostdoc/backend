package handlers

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strings"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/gorilla/websocket"
	"gitlab.com/f4lc09/logger"
)

var upgrader = websocket.Upgrader{
	ReadBufferSize:  1024,
	WriteBufferSize: 1024,
	CheckOrigin:     func(r *http.Request) bool { return true },
}

type documentState struct {
	DocRef  string   `json:"docRef"`
	Content string   `json:"content"`
	Changes []change `json:"changes"`
}

type change struct {
	Type    string `json:"type"`    // "insert" or "delete"
	Index   int    `json:"index"`   // Position in the document
	Content string `json:"content"` // Text content for insertion
}

type userConn struct {
	conn *websocket.Conn
	id   int64
}

type activeSession struct {
	users   map[int64]userConn
	message chan change
	join    chan userConn
	leave   chan userConn
	doc     documentState
}

var joinChansByActiveDocId = make(map[string]chan userConn)

// @Summary Handle WebSocket connections for real-time document editing.
// @Description This endpoint handles WebSocket connections, allowing users to collaborate in real-time on a shared document.
// @ID handleWebSocket
// @Produce json
// @Success 101 {string} string "Switching Protocols"
// @Router /ws [get]
func (h *docHandler) wshandler(c *gin.Context) {
	docRef := c.Param("doc_ref")

	if _, ok := joinChansByActiveDocId[docRef]; !ok {
		doc := documentState{
			DocRef:  docRef,
			Content: "",
			Changes: []change{},
		}

		users := make(map[int64]userConn)
		inChan := make(chan userConn)
		outChan := make(chan userConn)
		messages := make(chan change)

		session := activeSession{
			users:   users,
			join:    inChan,
			leave:   outChan,
			message: messages,
			doc:     doc,
		}

		joinChansByActiveDocId[docRef] = inChan
		go sessionWorker(session)
	}

	conn, err := upgrader.Upgrade(c.Writer, c.Request, nil)
	if err != nil {
		logger.Logf("ws: conn не апгрейднулось \n  %s ", err.Error())
		c.AbortWithStatus(http.StatusInternalServerError)
		return
	}

	_, m, err := conn.ReadMessage()
	if err != nil {
		logger.Debugf("error reading wb message: %v", err)
		return
	}

	tokenData, err := h.jwtService.ParseToken(string(m))
	if err != nil {
		logger.Error(err)
		conn.WriteJSON(err.Error())
		conn.Close()
		return
	}

	u := userConn{
		id:   tokenData.UserId,
		conn: conn,
	}

	joinChansByActiveDocId[docRef] <- u
}

func sessionWorker(session activeSession) {
	logger.Logf("initializing session with docRef %s", session.doc.DocRef)
	docId := strings.Split(session.doc.DocRef, "_")[0]
	t := time.NewTicker(time.Second * 10)

	for {
		select {
		case u := <-session.join:
			logger.Logf("doc %s: user %d joined the session", docId, u.id)
			session.users[u.id] = u
			go userConnectionHandler(u, session)
		case m := <-session.message:
			logger.Logf("doc %s: got message: \"%v\"", docId, m)
			if m.Type != "insert" && m.Type != "delete" {
				logger.Error(fmt.Errorf("неправильный тип операции, ожидались: insert, delete"))
			}

			if m.Type == "insert" {
				if len(session.doc.Content) < m.Index {
					m.Index = len(session.doc.Content)
				}

				// Плохо работает с сиволами занимающими больше одного байта (кирилица в том числе)
				start := session.doc.Content[:m.Index]
				end := session.doc.Content[m.Index:]

				session.doc.Content = start + m.Content + end
				session.doc.Changes = append(session.doc.Changes, m)
				for i := range session.users {
					session.users[i].conn.WriteJSON(session.doc.Content)
				}
			}
		case <-t.C:
			// fmt.Println("типо сохранение в бд")
			fmt.Println(session.doc)
			for i := range session.users {
				session.users[i].conn.WriteJSON(session.doc.Content)
			}
		}
	}
}

func userConnectionHandler(u userConn, session activeSession) {
	defer u.conn.Close()
	u.conn.WriteJSON(session.doc)
	for {
		var c change

		_, m, err := u.conn.ReadMessage()
		if err != nil {
			logger.Error(err)
			return
		}

		err = json.Unmarshal(m, &c)
		if err != nil {
			logger.Error(err)
		}

		session.message <- c
		time.Sleep(time.Second)
	}
}
