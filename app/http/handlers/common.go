package handlers

import (
	"io"
	"net/http"
	"os"
	"path/filepath"

	"github.com/gin-gonic/gin"
	swaggerFiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"
)

type commonHandler struct{}

func NewCommonHandler(router *gin.RouterGroup) *commonHandler {
	handler := &commonHandler{}

	router.Static("/static", "./static")
	router.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))
	router.GET("/download/:fileName", handler.downloadFile)
	router.GET("/ping", func(c *gin.Context) {
		c.JSON(http.StatusOK, gin.H{
			"message": "pong",
		})
	})

	return handler
}

func (h *commonHandler) downloadFile(c *gin.Context) {
	downloadName := c.Param("fileName")

	header := c.Writer.Header()
	header["Content-type"] = []string{"application/octet-stream"}
	header["Content-Disposition"] = []string{"attachment; filename= " + downloadName}

	file, err := os.Open(filepath.Join("./static", downloadName))
	if err != nil {
		c.String(http.StatusOK, "%v", err)
		return
	}
	defer file.Close()

	io.Copy(c.Writer, file)
}
