package handlers

import (
	"net/http"

	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	"gitlab.com/backend/gost-doc/app/models/errcore"
	jwt_models "gitlab.com/backend/gost-doc/app/models/jwt"
	"gitlab.com/backend/gost-doc/app/repository"
	"gitlab.com/f4lc09/logger"
)

func HeadersMiddleware() func(*gin.Context) {
	config := cors.DefaultConfig()
	config.AllowOrigins = []string{"*"}
	config.AllowCredentials = true
	config.AllowHeaders = []string{"*", "Authorization", "Content-Type"}
	config.AllowMethods = []string{"GET", "POST", "PUT", "DELETE", "OPTIONS"}
	return cors.New(config)
}

func AuthMiddleware(jwtService *jwt_models.JWTService) gin.HandlerFunc {
	return func(c *gin.Context) {
		jwtToken, err := jwtService.ExtractBearerToken(c.GetHeader("Authorization"))
		if err != nil {
			c.AbortWithStatusJSON(http.StatusUnauthorized, errcore.UnsignedResponse{
				Message: err.Error(),
			})
			return
		}

		token, err := jwtService.ParseToken(jwtToken)
		if err != nil {
			c.AbortWithStatusJSON(http.StatusUnauthorized, errcore.UnsignedResponse{
				Message: err.Error(),
			})
			return
		}
		c.Set("uid", token.UserId)
		c.Next()
	}
}

func SetUserAccess(db repository.Driver) gin.HandlerFunc {
	return func(c *gin.Context) {
		uid := c.GetInt64("uid")
		docRef := c.Query("doc_ref")

		dbResult, dbErr := db.ExecSP("user_role_by_doc_ref", uid, docRef)
		if dbErr != nil {
			setDBError(c, dbErr)
			c.Abort()
			return
		}

		urole := string(dbResult)
		c.Set("urole", urole)
		c.Next()
	}
}

func LogRequest() func(*gin.Context) {
	return func(c *gin.Context) {
		url := c.Request.URL.Path
		method := c.Request.Method
		c.Next()
		status := c.Writer.Status()
		logger.Logf("%s request to %s -> %d", method, url, status)
	}
}
