CREATE OR REPLACE FUNCTION create_user(_js JSON) RETURNS JSON
    LANGUAGE plpgsql AS
$$
DECLARE
    _id            INT;
    _email         VARCHAR(50);
    _username      VARCHAR(50);
    _password      VARCHAR(500);
    _error         VARCHAR(100) = '';
    _refresh_token UUID;
    _dt            TIMESTAMPTZ;
BEGIN
    _dt = NOW() AT TIME ZONE 'Europe/Moscow';

    SELECT email,
           username,
           password
    INTO _email,
         _username,
         _password
    FROM JSON_TO_RECORD(_js) AS s(email    VARCHAR(50),
                                  username VARCHAR(50),
                                  password VARCHAR(500));

    IF _email !~ '^[\w.-]+@([\w-]+\.)+[\w-]{2,4}$'
    THEN
        _error = 'поле "Email" не прошло проверку';
    END IF;

    IF _email = '' OR _email IS NULL THEN
        _error = 'поле "Email" пустое';
    ELSEIF _password = '' OR _password IS NULL THEN
        _error = 'поле "Пароль" пустое';
    ELSEIF _username = '' OR _username IS NULL THEN
        _error = 'поле "Имя пользователя" пустое';
    END IF;

    IF _error != '' THEN
        RAISE EXCEPTION '%' , _error;
    END IF;


    IF EXISTS(SELECT 1
              FROM users u
              WHERE lower(u.username) = lower(_username))
    THEN
        RAISE EXCEPTION 'имя пользователя % занято', _username;
    END IF;

    IF EXISTS(SELECT 1
              FROM users u
              WHERE lower(u.email) = lower(_email))
    THEN
        RAISE EXCEPTION 'почта % занята', _email;
    END IF;

    INSERT INTO users(email,
                      username,
                      password_hash)
    VALUES (_email,
            _username,
            md5(_password))
    RETURNING id
    INTO _id;

    INSERT INTO refresh_tokens(refresh_token,
                               user_id,
                               dt)
    VALUES (gen_random_uuid(),
            _id,
            _dt)
    RETURNING refresh_token
    INTO _refresh_token;

    RETURN json_build_object('id', _id, 'refresh_token', _refresh_token);
END;
$$;
