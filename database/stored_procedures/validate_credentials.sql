CREATE OR REPLACE FUNCTION validate_credentials(_js JSON) RETURNS JSON
    LANGUAGE plpgsql AS
$$
DECLARE
    _id             BIGINT;
    _login          VARCHAR(50);
    _password       VARCHAR(500);
    _password_hash  VARCHAR(500);
    _wrong_password BOOLEAN;
    _error          VARCHAR(100) = '';
    _refresh_token  UUID;
    _dt             TIMESTAMPTZ;
BEGIN
    _dt = NOW() AT TIME ZONE 'Europe/Moscow';
    RAISE NOTICE '%', _dt;

    SELECT login,
           password
    INTO _login,
         _password
    FROM JSON_TO_RECORD(_js) AS s(login    VARCHAR(50),
                                  password VARCHAR(500));

    IF _login = '' OR _login IS NULL THEN
        _error = 'необходимо указать Имя или Почту';
    ELSEIF _password = '' OR _password IS NULL THEN
        _error = 'необходимо ввести пароль';
    END IF;

    IF _error != '' THEN
        RAISE EXCEPTION '%' , _error;
    END IF;

    SELECT u.id,
           u.password_hash
    INTO _id,
         _password_hash
    FROM users u
    WHERE lower(u.username) = lower(_login)
       OR lower(u.email) = lower(_login);

    IF _password_hash != md5(_password)
    THEN
        _wrong_password = TRUE;
    END IF;

    IF _id IS NULL OR _wrong_password
    THEN
        RAISE EXCEPTION 'неверный логин или пароль';
    END IF;


    WITH del_cte AS (
        SELECT row_number() OVER (ORDER BY dt DESC) rn,
               r.id
        FROM refresh_tokens r
        WHERE r.user_id = _id
    )
    DELETE
    FROM refresh_tokens r
    USING del_cte c
    WHERE r.id = c.id
      AND c.rn > 4;

    INSERT INTO refresh_tokens(refresh_token,
                               user_id,
                               dt)
    VALUES (gen_random_uuid(),
            _id,
            _dt)
    RETURNING refresh_token
    INTO _refresh_token;

    RETURN json_build_object('id', _id, 'refresh_token', _refresh_token);
END;
$$;
