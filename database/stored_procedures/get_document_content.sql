CREATE OR REPLACE FUNCTION get_document_content(_doc_ref VARCHAR) RETURNS JSON
    LANGUAGE plpgsql AS
$$
DECLARE
    _doc_id        BIGINT;
    _res           JSON;
BEGIN
    -- добавить проверку доступа?
    SELECT get_doc_id_part(split_part(_doc_ref, '_', 1))
    INTO _doc_id;

    SELECT JSON_BUILD_OBJECT('type', dc.type,
                             'body', dc.body,
                             'next_content', dc.next_content)
    INTO _res
    FROM documents d
        JOIN document_content dc
            ON dc.id=d.content_start
    WHERE d.id=_doc_id;

    RETURN _res;
END;
$$;

CREATE OR REPLACE FUNCTION get_doc_id_part(_in VARCHAR(20)) RETURNS BIGINT
  LANGUAGE plpgsql AS
$$
DECLARE
    _out BIGINT;
BEGIN
   SELECT _in::BIGINT
   INTO  _out;
   RETURN _out;
   EXCEPTION WHEN others THEN
       RAISE EXCEPTION 'Некорректная ссылка';
END
$$;
