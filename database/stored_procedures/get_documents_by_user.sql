CREATE OR REPLACE FUNCTION get_documents_by_user(_user_id BIGINT) RETURNS JSON
    LANGUAGE plpgsql AS
$$
DECLARE
    _res           JSON;
    _dt            TIMESTAMPTZ  = NOW();
BEGIN
    _dt = NOW() AT TIME ZONE 'Europe/Moscow';

    WITH groups AS (
        SELECT ag.doc_id,
               r.name role_name
        FROM access_groups ag
            JOIN roles r
                ON r.id = ag.role_id
        WHERE ag.user_id = _user_id)
    SELECT JSON_AGG(JSON_BUILD_OBJECT(
                       'doc_ref', res.doc_ref,
                       'role_name', res.role_name,
                       'title', res.title,
                       'users', (SELECT jsonb_agg(row_to_json(users))
                                 FROM (SELECT u.id,
                                              u.username
                                              -- u.picture на будущее
                                       FROM users u
                                                JOIN access_groups ag
                                                    ON u.id=ag.user_id
                                            WHERE ag.doc_id=res.doc_id) users)
                   ))
    INTO _res
    FROM (SELECT d.id doc_id,
                 g.role_name,
                 d.title,
                 d.doc_ref
          FROM documents d
              JOIN groups g
                  ON d.id = g.doc_id) res;

    RETURN _res;
END;
$$;
