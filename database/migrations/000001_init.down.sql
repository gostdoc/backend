DROP TABLE IF EXISTS document_white_list;

DROP TABLE IF EXISTS access_groups;

DROP TABLE IF EXISTS documents;

DROP TABLE IF EXISTS document_config;

DROP TABLE IF EXISTS document_content;

DROP TABLE IF EXISTS document_status;

DROP TABLE IF EXISTS roles;

DROP TABLE IF EXISTS refresh_tokens;

DROP TABLE IF EXISTS users;
