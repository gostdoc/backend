package main

import (
	"flag"

	"gitlab.com/backend/gost-doc/app/config/yaml_config"
	"gitlab.com/backend/gost-doc/app/http"
	config_models "gitlab.com/backend/gost-doc/app/models/config"
	db "gitlab.com/backend/gost-doc/app/repository/postgresql"
	"gitlab.com/f4lc09/logger"

	_ "gitlab.com/backend/gost-doc/docs"
)

// @title           GOST_doc API
// @version         0.1
// @description     API documentation for GOST_doc web app.

//	@securityDefinitions.apikey	ApiKeyAuth
//	@in							header
//	@name						Authorization
//	@description		Данные нужно вводить в формате `Bearer AuthToken`

func main() {
	logger.InitLogger()
	configPath := flag.String("c", "./config.yaml", "a path for config file")
	flag.Parse()

	cfg := yaml_config.NewYamlConfig(&config_models.Main{})
	cfg.ReadFromFile(*configPath)

	db := db.NewPostgresqlRepository()
	db.InitConnection(cfg)
	defer db.Close()

	gostDocServer := http.NewGostDocServer(cfg, db)
	gostDocServer.Serve()
}
